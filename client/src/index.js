import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {Elements} from '@stripe/react-stripe-js';
import {loadStripe} from '@stripe/stripe-js';

import CheckoutForm from './CheckoutForm';

// Calling `loadStripe` outside of all components' render to avoid
// recreating the `Stripe` object on every render.
// NOTE: For production, save public key as environmental variable,
// rather than hardcoding it into the code as on the next line.
const stripePromise = loadStripe("pk_test_Ypc1MS3nZ9RFOs8AgcJ7QVk300qnUCbJ8M");

ReactDOM.render(
  <React.StrictMode>
    <App stripe={stripePromise}/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
