import React, { Component } from 'react';
import catMeme from './assets/lede-cat-memes-1300x890.jpg';
import './App.css';

import {Elements} from '@stripe/react-stripe-js';
import {loadStripe} from '@stripe/stripe-js';
import CardSection from './CardSection';
import CheckoutForm from './CheckoutForm';

class App extends Component {

  render() {
    return (
      <div className="App">
        <body>
          <img src={catMeme} className="App-logo" alt="logo" />
          <h1 className="App-title">Cat Meme T-shirts</h1>
          <h2>$10 each</h2>
          <div className="Card-section-container">
            <Elements stripe={this.props.stripe}>
              <CheckoutForm />
            </Elements>
          </div>
        </body>
      </div>
    );
  }
}

export default App;